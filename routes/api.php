<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'],function (){
    Route::group(['prefix' => 'company'],function (){
        Route::get('/','Api\v1\CompanyController@index');
        Route::post('/','Api\v1\CompanyController@store');
        Route::get('/{id}','Api\v1\CompanyController@show');
        Route::post('/update/{id}','Api\v1\CompanyController@update');
        Route::post('/delete/{id}','Api\v1\CompanyController@destroy');
    });
    Route::group(['prefix' => 'employee'],function (){
        Route::get('/','Api\v1\EmployeeController@index');
        Route::post('/','Api\v1\EmployeeController@store');
        Route::get('/{id}','Api\v1\EmployeeController@show');
        Route::post('/delete/{id}','Api\v1\EmployeeController@destroy');
    });
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
