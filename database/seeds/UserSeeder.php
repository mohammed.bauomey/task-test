<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = "super_admin";
        $user->password = Hash::make('password');
        $user->email = "super_admin@app.com";
        $user->is_admin = 1;
        $user->save();
    }
}
