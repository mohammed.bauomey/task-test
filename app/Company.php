<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name'];
    protected $table = 'companies';

    public function employee(){
        return $this->hasMany(Employee::class);
    }

    public function images()
    {
        return $this->morphOne(Image::class ,'imageable', 'model_type', 'model_id');
    }
}
