<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    public function company(){
        return $this->belongsTo(Company::class);
    }
    public function images()
    {
        return $this->morphOne(Image::class ,'imageable', 'model_type', 'model_id');
    }

}
