<?php

namespace App\Http\Controllers\Api\v1;

use App\Company;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Http\Resources\CompanyResources;
use App\Image;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $page = ($request->has("page") ?$request->page : 1)  ;
            $company = Company::paginate(5, ['*'], 'page', $page);
            return CompanyResources::collection($company);

        }catch(\Exception $e){
            return response()->json([
                'error' => 'There is error while show all companies '
            ]);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        try{
            //create Main Company Data
            $company = new Company(['name' => $request->name]);
            $company->save();

            //upload company image
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

            // add image to company model
            $imageable = new Image();
            $imageable->image_path = $imageName;
            $imageable->model_id = $company->id;
            $imageable->model_type = 'App\Company';
            $company->images()->save($imageable);
            return new CompanyResources(Company::find($company->id));
        }catch(\Exception $e){
            return response()->json([
                'error' => 'There is error while Create company'
            ]);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            return new CompanyResources(Company::find($id));
        }catch(\Exception $e){
            return response()->json([
                'error' => 'There is error while show company'
            ]);
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg',
                'name' =>'required'
            ]);
            $company =  Company::findOrFail($id);
            if($request->has('name')){
                $company->name =$request->name;
            }
            if($request->hasfile('image')){
                //upload company image
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images'), $imageName);

                // add image to company model
                Image::where(['model_id'=>$id,'model_type'=>'App\Company'])
                    ->update(['image_path'=>$imageName]);
            }
            $company->save();
            return new CompanyResources(Company::find($id));

        }catch(\Exception $e){
            return response()->json([
                'error' => 'There is error while update company'.$e
            ]);

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Company::destroy($id);
            return response()->json([
                'message' => 'Successfully deleted Company!'
            ]);

        }catch(\Exception $e){
            return response()->json([
                'error' => 'There is error while delete company'
            ]);

        }

    }
}
