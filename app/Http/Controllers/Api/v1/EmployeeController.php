<?php

namespace App\Http\Controllers\Api\v1;

use App\Company;
use App\Employee;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Http\Resources\EmployeeResources;
use App\Image;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $page = ($request->has("page") ?$request->page : 1)  ;
            $company = Employee::paginate(5, ['*'], 'page', $page);
            return EmployeeResources::collection($company);

        }catch(\Exception $e){
            return response()->json([
                'error' => 'There is error while show all Employees '
            ]);

        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        try {

            //create Main Company Data
            $employee = new Employee();
            $employee->name = $request->name;
            $employee->company_id = $request->company_id;
            $employee->save();

            //upload company image
            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

            // add image to company model
            $imageable = new Image();
            $imageable->image_path = $imageName;
            $imageable->model_id = $employee->id;
            $imageable->model_type = 'App\Employee';
            $employee->images()->save($imageable);
            return new EmployeeResources(Employee::find($employee->id));
        }catch (\Exception $e){
            return response()->json([
                'error' => 'There is error while Update Employee '
            ]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            return new EmployeeResources(Employee::find($id));
        }catch(\Exception $e){
            return response()->json([
                'error' => 'There is error while show employee'
            ]);
        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            ]);
            $employee =  Employee::findOrFail($id);
            if($request->has('name')){
                $employee->name = $request->name;
            }
            if($request->has('company_id')){
                $employee->company_id = $request->company_id;
            }
            $employee->save();
            if($request->hasfile('image')){
                //upload company image
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images'), $imageName);

                // add image to company model
                Image::where(['model_id'=>$id,'model_type'=>'App\Employee'])
                    ->update(['image_path'=>$imageName]);
            }
            return new EmployeeResources(Employee::find($id));
        }catch(\Exception $e){
            return response()->json([
                'error' => 'There is error while show employee'
            ]);
        }


    }
    public function destroy($id)
    {
        try{
            Employee::destroy($id);
            return response()->json([
                'message' => 'Successfully deleted Company!'
            ]);

        }catch(\Exception $e){
            return response()->json([
                'error' => 'There is error while delete company'
            ]);

        }

    }

}
