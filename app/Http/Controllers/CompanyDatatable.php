<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CompanyDatatable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request){

        $company = Company::with('images');
        return DataTables::of($company)
            ->editColumn('images', function (Company $com){
                if($com->images){
                    return '<img src='.asset('/images/'.$com->images->image_path).' alt="Test" height="42" width="42">';
                }
            })
            ->addColumn('action', function($data) {
                return '<a href="'.route('company.edit',[$data->id]).'" class="btn btn-dark">Update</a> || '.
                    '<button type="button" data-id="'.$data->id.'"  class="btn btn-danger btn-delete"
                        data-toggle="modal" data-animation="bounce" data-target=".delete-model-'.$data->id.'">Delete</button>
                        <div class="modal fade delete-model-'.$data->id.'" id="#delete-model-'.$data->id.'" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title mt-0" id="mySmallModalLabel">Delete Company</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <p>Are You Sure you want delete this Company ?</p>
                                </div>
                                <div class="modal-footer">
                                <form action="'.route("company.destroy",[$data->id]).'" method="POST">
                                    '.method_field("DELETE").'
                                    '.csrf_field().'
                                 <button type="form" class="btn btn-primary btn-submit-delete">Confirm Delete</button>
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </form>
                                </div>

                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
';
            })
            ->rawColumns(['images','action'])
            ->make(true);
    }
}
