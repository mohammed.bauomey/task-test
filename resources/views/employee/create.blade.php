@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-4">
                                <h3>Create Employee</h3>
                            </div>
                        </div>
                    </div>
                    <br>
                    @if (count($errors) > 0)

                        <div class="alert alert-danger">

                            <strong>Whoops!</strong> There were some problems with your input.

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif

                    <div class="panel-body">
                        <form action="{{route('employee.store')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInput1">Employee Name</label>
                                <input type="text" name="name" class="form-control" id="exampleInput1"  placeholder="Enter Company Name" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInput1">Company Name</label>
                                <select name="company_id" class="form-control">
                                    <option value="" selected>Select Your Company</option>
                                    @foreach($company as $com)
                                        <option value="{{$com->id}}">{{$com->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Image file input</label>
                                <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
