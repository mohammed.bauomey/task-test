@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-4">
                                <h3>Update Employee</h3>
                            </div>
                        </div>
                    </div>
                    <br>
                    @if (count($errors) > 0)

                        <div class="alert alert-danger">

                            <strong>Whoops!</strong> There were some problems with your input.

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif

                    <div class="panel-body">
                        <form action="{{route('employee.update',[$employee->id])}}" method="post" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInput1">Employee Name</label>
                                <input type="text" name="name" class="form-control" id="exampleInput1" value="{{$employee->name}}" placeholder="Enter Company Name" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInput1">Company Name</label>
                                <select name="company_id" class="form-control">
                                    @foreach($company as $com)

                                        <option
                                                @if($employee->company_id == $com->id) selected @endif
                                                value="{{$com->id}}">
                                                    {{$com->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Image file input</label>
                                <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                                <img src="{{asset('/images/'.$employee->images->image_path)}}" alt="" width="150" height="150">

                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
