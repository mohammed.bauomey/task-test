@extends('layouts.app')
@section('style')
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-4">
                                <h3>Company Datatable</h3>
                            </div>
                            <div class="col-md-4">
                                <a href="{{route('company.create')}}" class="btn btn-dark" >Create Company</a>
                            </div>
                        </div>
                     </div>
                    <br>


                    <div class="panel-body">
                        <table class="table table-hover table-bordered table-striped datatable" style="width:100%">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>image</th>
                                <th>action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>    </div>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.noConflict();
            console.log('test')
            $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('companyDatatable') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'images', name: 'images',orderable: false, searchable:false },
                    {data: 'action', name: 'action',orderable: false, searchable:false },
                ]
            });
        });
    </script>
@endsection
