@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-4">
                                <h3>Edit Company</h3>
                            </div>
                        </div>
                    </div>
                    <br>
                    @if (count($errors) > 0)

                        <div class="alert alert-danger">

                            <strong>Whoops!</strong> There were some problems with your input.

                            <ul>

                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>

                        </div>

                    @endif

                    <div class="panel-body">
                        <form action="{{route('company.update',[$company->id])}}" method="post" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInput1">Company Name</label>
                                <input type="text" name="name" class="form-control" id="exampleInput1"  placeholder="Enter Company Name" value="{{$company->name}}" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Image file input</label>
                                <input type="file" name="image" value="" class="form-control-file" id="exampleFormControlFile1">
                                <img src="{{asset('/images/'.$company->images->image_path)}}" alt="" width="150" height="150">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
