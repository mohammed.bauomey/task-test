@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <a href="{{route('company.index')}}" class="btn btn-dark">Company Data</a>
                        <a href="{{route('employee.index')}}" class="btn btn-dark">Employee Data</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
